package com.example.damir.lv3d;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class YouSureDialog extends DialogFragment {

    private String mTextMessage, TextColor;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle("Promjeni tekst?")
                .setMessage("Stisni Yes ako zelis promjenit tekst")
                .setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //EditFragment changeMyTextObj = new EditFragment();
                                //changeMyTextObj.changeMyText();
                            }
                        })
                .setNegativeButton(
                        "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getActivity(),"Canceled",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                );
        return dialogBuilder.create();
    }
}
