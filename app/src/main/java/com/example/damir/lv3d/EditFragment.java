package com.example.damir.lv3d;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

public class EditFragment extends Fragment {

    private int mTextSize = 10;
    private String mTextMessage = "...";
    private EditText etTextMessage;
    private SeekBar sbTextSize;
    private Button bChangeText;
    private TextChangeListener mTextChangeListener;
    private Spinner spinnerColor;
    private String TextColor;
    private final String DIALOG_TAG = "YouSureDialog";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View Layout = inflater.inflate(R.layout.fragment_edit,null);
        setUpUI(Layout);
        return Layout;
    }

    private void setUpUI(View layout) {
        this.etTextMessage = (EditText) layout.findViewById(R.id.etMessage);
        this.sbTextSize = (SeekBar) layout.findViewById(R.id.sbTextSize);
        this.bChangeText = (Button) layout.findViewById(R.id.btnChangeText);
        this.spinnerColor = (Spinner) layout.findViewById(R.id.spinnerColor);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.colors));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerColor.setAdapter(myAdapter);
        this.sbTextSize.setProgress(this.mTextSize);
        this.bChangeText.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        YouSureDialog youSureDialog = new YouSureDialog();
                        youSureDialog.show(getFragmentManager(), DIALOG_TAG);
                        mTextMessage = etTextMessage.getText().toString();
                        mTextSize = sbTextSize.getProgress();
                        TextColor = spinnerColor.getSelectedItem().toString();
                        mTextChangeListener.onTextChange(mTextSize,mTextMessage,TextColor);
                    }
                }
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof TextChangeListener)
        {
            this.mTextChangeListener = (TextChangeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mTextChangeListener = null;
    }

    public interface TextChangeListener
    {
        void onTextChange(int TextSize, String TextMessage, String TextColor);
    }

    public void changeMyText (){
        mTextMessage = etTextMessage.getText().toString();
        mTextSize = sbTextSize.getProgress();
        TextColor = spinnerColor.getSelectedItem().toString();
        mTextChangeListener.onTextChange(mTextSize,mTextMessage,TextColor);
    }
}

