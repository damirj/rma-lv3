package com.example.damir.lv3d;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DisplayFragment extends Fragment {

    TextView tvMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View Layout = inflater.inflate(R.layout.fragment_display,null);
        this.tvMessage = (TextView) Layout.findViewById(R.id.tvMessage);
        return Layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setText(int TextSize, String TextMessage, String TextColor)
    {
        switch (TextColor){
            case "BLUE":
                this.tvMessage.setTextColor(Color.BLUE);
                break;
            case "GREEN":
                this.tvMessage.setTextColor(Color.GREEN);
                break;
            case "YELLOW":
                this.tvMessage.setTextColor(Color.YELLOW);
                break;
        }
        this.tvMessage.setText(TextMessage);
        this.tvMessage.setTextSize(TextSize);
    }
}
